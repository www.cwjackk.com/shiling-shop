<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html>
<head>
	<!-- css和js文件 -->
	<meta charset="UTF-8">
<title><?php echo C('WEB_SITE_TITLE');?></title>
<link href="/Public/static/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/Public/static/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<link href="/Public/static/bootstrap/css/docs.css" rel="stylesheet">
<link href="/Public/static/bootstrap/css/onethink.css" rel="stylesheet">
<link href="/Public/static/swiper/swiper-3.3.1.min.css" rel="stylesheet" >
<link href="/Public/static/bootstrap/css/font-awesome.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/Public/static/bootstrap/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="/Public/static/jquery-1.10.2.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/Public/static/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/Public/static/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/static/swiper/swiper-3.3.1.min.js"></script>

<!-- <script src="http://yui.yahooapis.com/3.5.1/build/yui/yui-min.js"></script> -->
<!--<![endif]-->
<!-- 页面header钩子，一般用于加载插件CSS文件和代码 -->
<?php echo hook('pageHeader');?>

</head>
<body>
	<!-- 头部 -->
	<!-- 导航条
================================================== -->
<div class="navbar navbar-default navbar-static-top">
    <div class="navbar-inner" style="box-shadow: 0 1px 10px rgba(0, 0, 0, 0);border: 0px solid #d4d4d4;">
        <div class="container">
            <div class="logo-box" style="float: left;width: 55px;padding:10px 10px 10px 0;">
                <img src="/Public/static/bootstrap/img/logo.jpg" class="img-circle">
            </div>
            <div class="title-box" style="height:auto;float:left;">
                <a class="brand" href="<?php echo U('index/index');?>" style="padding-left: 20px;color: gray;line-height: 55px;">时令购 SeasonShop</a>
            </div>
            <!-- <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <!-- <div class="nav-collapse collapse">
                <ul class="nav">
                    <?php $__NAV__ = M('Channel')->field(true)->where("status=1")->order("sort")->select(); if(is_array($__NAV__)): $i = 0; $__LIST__ = $__NAV__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i; if(($nav["pid"]) == "0"): ?><li>
                            <a href="<?php echo (get_nav_url($nav["url"])); ?>" target="<?php if(($nav["target"]) == "1"): ?>_blank<?php else: ?>_self<?php endif; ?>"><?php echo ($nav["title"]); ?></a>
                        </li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div> -->
            <!-- <div class="nav-collapse collapse pull-right">
                <?php if(is_login()): ?><ul class="nav" style="margin-right:0">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-left:0;padding-right:0"><?php echo get_username();?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo U('User/profile');?>">修改密码</a></li>
                                <li><a href="<?php echo U('User/logout');?>">退出</a></li>
                            </ul>
                        </li>
                    </ul>
                <?php else: ?>
                    <ul class="nav" style="margin-right:0">
                        <li>
                            <a href="<?php echo U('User/login');?>">登录</a>
                        </li>
                        <li>
                            <a href="<?php echo U('User/register');?>" style="padding-left:0;padding-right:0">注册</a>
                        </li>
                    </ul><?php endif; ?>
            </div> -->
        </div>
    </div>
</div>

	<!-- /头部 -->

	<!-- 主体 -->
	
    <header class=" subhead" id="overview">
        <div class="container">
            <div class="row" style="height:438px;overflow:hidden;">
                <!-- <div class="span3">
                    <h2>时令购</h2>
                    <h3>跟着时间来shopping</h3>
                </div> -->
                <div class="span9">
                    <div class="row">
                        <div class="span4 notice-list" style="padding-top:0%;">
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>相对湿度</span>
                                    <span>70<span>%</span></span>
                                    <i class="icon-flag icon-large " ></i>
                                </a>
                                <input type="hidden" name="shidu" value="湿度相对较大，做好防潮准备。">
                            </h5>
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>PM2.5</span>
                                    <span>良</span>
                                    <i class="icon-gift icon-large " ></i>
                                </a>
                                <input type="hidden" name="pm25" value="今天的空气质量是可以接受的，除少数异常敏感体质的人群外，大家可在户外正常活动。">
                            </h5>
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>紫外线</span>
                                    <span>弱</span>
                                    <i class="icon-adjust icon-large " ></i>
                                </a>
                                <input type="hidden" name="pm25" value="紫外线强度较弱，建议出门前涂擦SPF在12-15之间、PA+的防晒护肤品。">
                            </h5>
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>污染</span>
                                    <span>良</span>
                                    <i class="icon-heart icon-large " ></i>
                                </a>
                                <input type="hidden" name="pm25" value="气象条件有利于空气污染物稀释、扩散和清除，可在室外正常活动。">
                            </h5>
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>洗车</span>
                                    <span>不宜</span>
                                    <i class="icon-truck icon-large " ></i>
                                </a>
                                <input type="hidden" name="pm25" value="不宜洗车，未来24小时内有雨，如果在此期间洗车，雨水和路上的泥水可能会再次弄脏您的爱车。">
                            </h5>
                            <h5 class="weather-indexs">
                                <a href="#">
                                    <span>运动</span>
                                    <span>较不宜</span>
                                    <i class="icon-gift icon-large " ></i>
                                </a>
                                <input type="hidden" name="pm25" value="有降水，推荐您在室内进行健身休闲运动；若坚持户外运动，须注意保暖并携带雨具。">
                            </h5>
                            <div class="notice-box" >
                                提醒：
                                    <span>易感人群症状有轻度加剧，健康人群出现刺激症状。建议儿童、老年人及心脏病患者应减少长时间、高强度。</span><a href="#">生活小贴士</a>

                            </div>
                        </div>
                        <div class="span5">
                            <!-- 为 ECharts 准备一个具备大小（宽高）的Dom -->
                            <div id="echarts-box" style="width: auto;height:250px;"></div>
                            <div id="clothes-choose">
                                <ul class="nav nav-tabs">
                                    <li role="presentation" class="active"><a href="#">女士</a></li>
                                    <li role="presentation"><a href="#">男士</a></li>
                                    <li role="presentation"><a href="#">儿童</a></li>
                                    <li role="presentation"><a href="#">老人</a></li>
                                </ul>
                                <ul class="show-clouth">
                                    <li>
                                        <div class="clouth-img"></div>
                                        <div class="clouth-title">
                                            <span>长袖卫衣</span>
                                            <span>长袖卫衣</span>
                                            <span>长袖卫衣</span>
                                            <span>长袖卫衣</span>
                                        </div>
                                    </li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="span3">
                    <div class="date_box" style="border: 0px solid red;height: 100%;">
                        <div class="wannianli-card">
                            <div class="wannianli-info">
                                <h2>2016年06月11日 周六</h2>
                                <h1>11</h1>
                                <h3>农历五月初七</h3>
                                <h4>丙申年【猴年】 </h4>
                            </div>
                            <!-- <div class="wannianli-other">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><div>宜<i></i></div></td>
                                            <td>
                                                <span>造车器、纳采、订盟、祭祀、祈福、求嗣、移徙、出行、开市、出火、入宅、立券、交易、入宅、安门、安床、安葬、谢土</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><div>忌<i></i></div></td>
                                            <td><span>开光、造屋、动土、作灶、栽种</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> -->
                            <div class="wannianli-other solar-terms" style="background:#FF961E;opacity:0.9;">
                                <h2 style="color:white;text-align:center;">芒种</h2>
                                <span style="color:white;text-align:center;width:100%;">芒种初过雨及时 纱厨睡起角巾欹</span>
                            </div>
                            <i class="wannianli-ico wannianli-lt"></i>
                            <i class="wannianli-ico wannianli-rt"></i>
                            <i class="wannianli-ico wannianli-rb" style="box-shadow:-3px -3px 3px 1px rgba(0,0,0,.15);"></i>
                        </div>
                    </div>
                    <!-- <div class="solar-terms">
                        <img src="/Public/static/bootstrap/img/mangzhong.jpg" style="height:120px;">
                    </div> -->
                </div>
            </div>
            <p class="lead"></p>
        </div>
    </header>

<div id="main-container" class="container" style="margin-top: 10px;">
    <div class="row">
        
    <div class="span12 ">
        <!-- <ul class="nav nav-list bs-docs-sidenav">
            <li class="active">
                <a href=" ">
                    我是轮播图
                </a>
            </li>
        </ul> -->
        <div class="swiper-container lunbo1">
            <div class="underlayer" style="position:absolute;width:100%;height:160px;background:black;opacity:0.7;"></div>
            <div class="swiper-wrapper" style="padding:5px 0;">
                <div class="swiper-slide" style="height: 150px;width: 20%;overflow:hidden;">
                    <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" width="100%">
                </div>
                <div class="swiper-slide" style="height: 150px;width: 20%;overflow:hidden;opacity:0.5;">
                    <img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1xr5_KXXXXXa.XpXXXXXXXXXX_!!0-item_pic.jpg_220x220" width="100%">
                </div>
                <div class="swiper-slide" style="height: 150px;width: 20%;overflow:hidden;opacity:0.5;">
                    <img src="http://img03.taobaocdn.com/bao/uploaded/i3/671012022/TB2Z3IfoVXXXXaTXpXXXXXXXXXX_!!671012022.jpg_220x220" width="100%">
                </div>
                <div class="swiper-slide" style="height: 150px;width: 20%;overflow:hidden;opacity:0.5;">
                    <img src="http://img04.taobaocdn.com/bao/uploaded/i4/159229889/TB2Lu2IppXXXXaJXXXXXXXXXXXX_!!159229889.jpg_220x220" width="100%">
                </div>
                <div class="swiper-slide" style="height: 150px;width: 20%;background: pink;">slider5</div>
                <div class="swiper-slide" style="height: 150px;width: 20%;background: yellow;">slider6</div>
            </div>
        </div>
    </div>

        <!-- 内容部分 -->
        
    <div class="span12">
        <!-- Contents
        ================================================== -->
        <section id="contents">
            <!-- <?php $category=D('Category')->getChildrenId(1);$__LIST__ = D('Document')->page(!empty($_GET["p"])?$_GET["p"]:1,10)->lists($category, '`level` DESC,`id` DESC', 1,true); if(is_array($__LIST__)): $i = 0; $__LIST__ = $__LIST__;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$article): $mod = ($i % 2 );++$i;?><div class="">
                    <h3><a href="<?php echo U('Article/detail?id='.$article['id']);?>"><?php echo ($article["title"]); ?></a></h3>
                </div>
                <div>
                    <p class="lead"><?php echo ($article["description"]); ?></p>
                </div>
                <div>
                    <span><a href="<?php echo U('Article/detail?id='.$article['id']);?>">查看全文</a></span>
                    <span class="pull-right">
                        <span class="author"><?php echo (get_username($article["uid"])); ?></span>
                        <span>于 <?php echo (date('Y-m-d H:i',$article["create_time"])); ?></span> 发表在 <span>
                        <a href="<?php echo U('Article/lists?category='.get_category_name($article['category_id']));?>"><?php echo (get_category_title($article["category_id"])); ?></a></span> ( 阅读：<?php echo ($article["view"]); ?> )
                    </span>
                </div>
                <hr/><?php endforeach; endif; else: echo "" ;endif; ?> -->
            <div class="swiper-container lunbo2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-no-swiping" style="background-color: gray;">
                        <div class="row">
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://item.taobao.com/item.htm?id=533083289869" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                        <!-- <div class="serviceItem-other">
                                            <div class="col-xs-6 col-md-6">
                                                <span class="serviceItem-percent">3</span>分钟
                                                <br>
                                                <small>轻松下单</small>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <span class="serviceItem-remain">100</span>%
                                                <br>
                                                <small>原创写作</small>
                                            </div>
                                        </div> -->
                                    </div>
                                    <a class="project-item-btn biding" href="https://item.taobao.com/item.htm?id=533083289869" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://item.taobao.com/item.htm?id=533083289869" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://item.taobao.com/item.htm?id=533083289869" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://item.taobao.com/item.htm?id=533083289869" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://item.taobao.com/item.htm?id=533083289869" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://item.taobao.com/item.htm?id=533083289869" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://item.taobao.com/item.htm?id=533083289869" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img04.taobaocdn.com/bao/uploaded/i4/671012022/TB2EyRoqXXXXXXqXXXXXXXXXXXX_!!671012022.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://item.taobao.com/item.htm?id=533083289869" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://item.taobao.com/item.htm?id=533083289869" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 第二页 -->
                    <div class="swiper-slide swiper-no-swiping" style="height: 580px;background: blue;">
                        <div class="row">
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1xr5_KXXXXXa.XpXXXXXXXXXX_!!0-item_pic.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://detail.tmall.com/item.htm?id=532014047118" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://detail.tmall.com/item.htm?id=532014047118" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1xr5_KXXXXXa.XpXXXXXXXXXX_!!0-item_pic.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://detail.tmall.com/item.htm?id=532014047118" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://detail.tmall.com/item.htm?id=532014047118" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1xr5_KXXXXXa.XpXXXXXXXXXX_!!0-item_pic.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://detail.tmall.com/item.htm?id=532014047118" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://detail.tmall.com/item.htm?id=532014047118" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                            <div class="span3 span-box">
                                <div class="servie-item-box">
                                    <div class="details center-block">
                                        <div class="serviceItem-brand">
                                            <span>时令女装</span>
                                            <span>质量保证</span>
                                        </div>
                                        <div class="img-box">
                                            <img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1xr5_KXXXXXa.XpXXXXXXXXXX_!!0-item_pic.jpg_220x220" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。" class="img-rounded">
                                        </div>
                                        <p>MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。
                                            <a href="https://detail.tmall.com/item.htm?id=532014047118" title="MIUCO女装2016夏季新款典雅印花双层喇叭袖收腰系带真丝连衣裙。">详情→</a>
                                        </p>
                                    </div>
                                    <a class="project-item-btn biding" href="https://detail.tmall.com/item.htm?id=532014047118" target="_blank">立即购买</a>
                                    <div class="section-bottom-a"></div>
                                    <div class="section-bottom-b"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-no-swiping" style="height: 580px;background: orange;">slider3</div>
                    <div class="swiper-slide swiper-no-swiping" style="height: 580px;background: purple;">slider4</div>
                    <div class="swiper-slide swiper-no-swiping" style="height: 580px;background: pink;">slider5</div>
                    <div class="swiper-slide swiper-no-swiping" style="height: 580px;background: yellow;">slider6</div>
                </div>
                <div class="swiper-button-prev swiper-button-black"></div>
                <div class="swiper-button-next swiper-button-black"></div>
            </div>
        </section>
    </div>
    <!-- 图表JS -->
    <script type="text/javascript" src="/Public/static/echarts/echarts.min.js"></script>
    <!-- 轮播图JS -->
    <script type="text/javascript">
    $(function(){
        swiperStart();//轮播图
        init_echarts();//初始化echarts
        bind_event();//绑定的事件
    })
    function swiperStart(){
        var Swiper1 = new Swiper('.lunbo1', {
            //autoplay: 3000,//可选选项，自动滑动
            slidesPerView : 'auto',//显示个数
            slidesPerGroup : 1,//滑动个数
            // controlBy:'slide',
            centeredSlides: true,
            touchRatio: 0.2,//触摸距离与slide滑动距离的比率。
            // loop : true,
            spaceBetween: 10,//间距
        })
        var Swiper2 = new Swiper('.lunbo2', {
            noSwiping : true,
            prevButton:'.swiper-button-prev',
            nextButton:'.swiper-button-next',
            // loop : true,
            spaceBetween: 10,
        })
        Swiper1.params.control = Swiper2;//需要在Swiper2初始化后，Swiper1控制Swiper2
        Swiper2.params.control = Swiper1;//需要在Swiper1初始化后，Swiper2控制Swiper1
    }
    function bind_event(){
        $(".notice-list h5").mousemove(function(){
            var text = $(this).find('input').val();
            $(".notice-box span").text(text);
            // alert(text);
        })

        $(".nav-tabs").on('click', 'li', function(event) {
            $(this).addClass('active').siblings('li').removeClass();
        });
    }
    function init_echarts(){
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echarts-box'));
        // 指定图表的配置项和数据
        var option = {
            title: {
                text: "未来一周气温变化",
                subtext: "实时数据",
                textStyle: {
                    fontWeight: "lighter"
                }
            },
            tooltip: {
                trigger: "axis",
                show: true
            },
            legend: {
                data: ["一周气温变化"]
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {
                        show: true
                    },
                    dataView: {
                        show: true,
                        readOnly: true
                    },
                    magicType: {
                        show: false,
                        type: ["line", "bar"]
                    },
                    restore: {
                        show: true
                    },
                    saveAsImage: {
                        show: true
                    }
                }
            },
            calculable: false,
            xAxis: [
                {
                    type: "category",
                    boundaryGap: false,
                    data: ["6-13", "6-14", "6-15", "6-16", "6-17", "6-18", "6-19"],
                    boundaryGap: true
                }
            ],
            yAxis: [
                {
                    type: "value",
                    name: "°C",
                    axisLine: {
                        show: false
                    }
                }
            ],
            series: [
                {
                    name: "平均气温度",
                    type: "line",
                    data: [11, 11, 20, 13, 12, 21, 10],
                    smooth: true,
                    symbolSize: 3,
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: "default",
                                color: "rgba(183, 183, 183, 0.7)"
                            },
                            label: {
                                position: "top",
                                show: true,
                                formatter: "{c}°C",
                                textStyle: {
                                    color: "rgba(68, 68, 68, 0.9)"
                                }
                            },
                            color: "rgba(91, 91, 91, 0.7)"
                        }
                    }
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    }
    </script>

    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(window).resize(function(){
            $("#main-container").css("min-height", $(window).height() - 343);
        }).resize();
    })
</script>

	<!-- /主体 -->

	<!-- 底部 -->
	
    <!-- 底部
    ================================================== -->
    <footer class="footer">
      <div class="container">
          <p> 本站由 <strong><a href="http://www.onethink.cn" target="_blank">OneThink</a></strong> 强力驱动</p>
      </div>
    </footer>

<script type="text/javascript">
(function(){
	var ThinkPHP = window.Think = {
		"ROOT"   : "", //当前网站地址
		"APP"    : "/index.php?s=", //当前项目地址
		"PUBLIC" : "/Public", //项目公共目录地址
		"DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>", //PATHINFO分割符
		"MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
		"VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
	}
})();
</script>
 <!-- 用于加载js代码 -->
<!-- 页面footer钩子，一般用于加载插件JS文件和JS代码 -->
<?php echo hook('pageFooter', 'widget');?>
<div class="hidden"><!-- 用于加载统计代码等隐藏元素 -->
	
</div>

	<!-- /底部 -->
</body>
</html>