-- -----------------------------
-- Think MySQL Data Transfer 
-- 
-- Host     : 127.0.0.1
-- Port     : 3306
-- Database : onethink
-- 
-- Part : #1
-- Date : 2016-06-13 15:49:59
-- -----------------------------

SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------
-- Table structure for `onethink_action`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_action`;
CREATE TABLE `onethink_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行为说明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text NOT NULL COMMENT '行为规则',
  `log` text NOT NULL COMMENT '日志规则',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统行为表';

-- -----------------------------
-- Records of `onethink_action`
-- -----------------------------
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action` VALUES ('', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_action_log`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_action_log`;
CREATE TABLE `onethink_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行为id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行用户id',
  `action_ip` bigint(20) NOT NULL COMMENT '执行行为者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '触发行为的数据id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';

-- -----------------------------
-- Records of `onethink_action_log`
-- -----------------------------
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_action_log` VALUES ('', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_addons`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_addons`;
CREATE TABLE `onethink_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- -----------------------------
-- Records of `onethink_addons`
-- -----------------------------
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_addons` VALUES ('', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_attachment`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_attachment`;
CREATE TABLE `onethink_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件显示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件类型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '资源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联记录ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上级目录ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表';


-- -----------------------------
-- Table structure for `onethink_attribute`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_attribute`;
CREATE TABLE `onethink_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段注释',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定义',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '数据类型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默认值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='模型属性表';

-- -----------------------------
-- Records of `onethink_attribute`
-- -----------------------------
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_attribute` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_auth_extend`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_extend`;
CREATE TABLE `onethink_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用户id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '扩展表中数据的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '扩展类型标识 1:栏目分类权限;2:模型权限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组与分类的对应关系表';

-- -----------------------------
-- Records of `onethink_auth_extend`
-- -----------------------------
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');
INSERT INTO `onethink_auth_extend` VALUES ('', '', '');

-- -----------------------------
-- Table structure for `onethink_auth_group`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_group`;
CREATE TABLE `onethink_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '用户组所属模块',
  `type` tinyint(4) NOT NULL COMMENT '组类型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_auth_group`
-- -----------------------------
INSERT INTO `onethink_auth_group` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_group` VALUES ('', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_auth_group_access`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_group_access`;
CREATE TABLE `onethink_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `onethink_auth_rule`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_auth_rule`;
CREATE TABLE `onethink_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜单',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_auth_rule`
-- -----------------------------
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');
INSERT INTO `onethink_auth_rule` VALUES ('', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_category`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_category`;
CREATE TABLE `onethink_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(30) NOT NULL COMMENT '标志',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每页行数',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的网页标题',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '频道页模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表页模板',
  `template_detail` varchar(100) NOT NULL COMMENT '详情页模板',
  `template_edit` varchar(100) NOT NULL COMMENT '编辑页模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '关联模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允许发布的内容类型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许发布内容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可见性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许回复',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '发布的文章是否需要审核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '扩展设置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类图标',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- -----------------------------
-- Records of `onethink_category`
-- -----------------------------
INSERT INTO `onethink_category` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_category` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_category` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_channel`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_channel`;
CREATE TABLE `onethink_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '频道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级频道ID',
  `title` char(30) NOT NULL COMMENT '频道标题',
  `url` char(100) NOT NULL COMMENT '频道连接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打开',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_channel`
-- -----------------------------
INSERT INTO `onethink_channel` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_channel` VALUES ('', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_config`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_config`;
CREATE TABLE `onethink_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_config`
-- -----------------------------
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_config` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_document`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document`;
CREATE TABLE `onethink_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '标题',
  `category_id` int(10) unsigned NOT NULL COMMENT '所属分类',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '内容类型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推荐位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可见性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至时间',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件数量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '扩展统计字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '优先级',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文档模型基础表';

-- -----------------------------
-- Records of `onethink_document`
-- -----------------------------
INSERT INTO `onethink_document` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_document` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_document_article`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document_article`;
CREATE TABLE `onethink_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '文章内容',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `bookmark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型文章表';

-- -----------------------------
-- Records of `onethink_document_article`
-- -----------------------------
INSERT INTO `onethink_document_article` VALUES ('', '', '', '', '');
INSERT INTO `onethink_document_article` VALUES ('', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_document_download`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document_download`;
CREATE TABLE `onethink_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '下载详细描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型下载表';


-- -----------------------------
-- Table structure for `onethink_file`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_file`;
CREATE TABLE `onethink_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` int(10) unsigned NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';


-- -----------------------------
-- Table structure for `onethink_hooks`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_hooks`;
CREATE TABLE `onethink_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_hooks`
-- -----------------------------
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');
INSERT INTO `onethink_hooks` VALUES ('', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_member`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_member`;
CREATE TABLE `onethink_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵称',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq号',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会员状态',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- -----------------------------
-- Records of `onethink_member`
-- -----------------------------
INSERT INTO `onethink_member` VALUES ('', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_menu`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_menu`;
CREATE TABLE `onethink_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分组',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否仅开发者模式可见',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Records of `onethink_menu`
-- -----------------------------
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_menu` VALUES ('', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_model`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_model`;
CREATE TABLE `onethink_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型标识',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名称',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '继承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '继承与被继承模型的关联字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表时是否需要主键字段',
  `field_sort` text NOT NULL COMMENT '表单字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基础' COMMENT '字段分组',
  `attribute_list` text NOT NULL COMMENT '属性列表（表的字段）',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '编辑模板',
  `list_grid` text NOT NULL COMMENT '列表定义',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表数据长度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默认搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高级搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '数据库引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='文档模型表';

-- -----------------------------
-- Records of `onethink_model`
-- -----------------------------
INSERT INTO `onethink_model` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_model` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `onethink_model` VALUES ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_picture`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_picture`;
CREATE TABLE `onethink_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- -----------------------------
-- Table structure for `onethink_ucenter_admin`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_admin`;
CREATE TABLE `onethink_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员用户ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理员状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员表';


-- -----------------------------
-- Table structure for `onethink_ucenter_app`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_app`;
CREATE TABLE `onethink_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应用ID',
  `title` varchar(30) NOT NULL COMMENT '应用名称',
  `url` varchar(100) NOT NULL COMMENT '应用URL',
  `ip` char(15) NOT NULL COMMENT '应用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陆',
  `allow_ip` varchar(255) NOT NULL COMMENT '允许访问的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '应用状态',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='应用表';


-- -----------------------------
-- Table structure for `onethink_ucenter_member`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_member`;
CREATE TABLE `onethink_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `mobile` char(15) NOT NULL COMMENT '用户手机',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- -----------------------------
-- Records of `onethink_ucenter_member`
-- -----------------------------
INSERT INTO `onethink_ucenter_member` VALUES ('', '', '', '', '', '', '', '', '', '', '');

-- -----------------------------
-- Table structure for `onethink_ucenter_setting`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_ucenter_setting`;
CREATE TABLE `onethink_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型（1-用户配置）',
  `value` text NOT NULL COMMENT '配置数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='设置表';


-- -----------------------------
-- Table structure for `onethink_url`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_url`;
CREATE TABLE `onethink_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '链接唯一标识',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短网址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='链接表';


-- -----------------------------
-- Table structure for `onethink_userdata`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_userdata`;
CREATE TABLE `onethink_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '类型标识',
  `target_id` int(10) unsigned NOT NULL COMMENT '目标id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

